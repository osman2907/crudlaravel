<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $trademarks = [
            [
                'id' => 1,
                'name' => 'Nike',
            ],
            [
                'id' => 2,
                'name' => 'Adidas',
            ],
            [
                'id' => 3,
                'name' => 'Puma',
            ]
        ];

        $categories = [
            [
                'id' => 1,
                'name' => 'Calzado Deportivo Damas',
            ],
            [
                'id' => 2,
                'name' => 'Calzado Deportivo Caballeros',
            ],
            [
                'id' => 3,
                'name' => 'Calzado Casual Damas',
            ],
            [
                'id' => 4,
                'name' => 'Calzado Casual Caballeros',
            ]
        ];
        DB::table('trademarks')->insert($trademarks);
        DB::table('categories')->insert($categories);
    }
}
