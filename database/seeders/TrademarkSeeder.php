<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TrademarkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trademarks = [
            [
                'id' => 1,
                'name' => 'Nike',
            ],
            [
                'id' => 2,
                'name' => 'Adidas',
            ],
            [
                'id' => 3,
                'name' => 'Puma',
            ]
        ];
        DB::table('trademarks')->insert($trademarks);
    }
}
