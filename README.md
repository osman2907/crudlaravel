<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Evaluación Osman Pérez

CRUD básico para evaluación.

### Instalación del proyecto

- Clonar el proyecto.
- Ubicarse en la raíz del proyecto.
- Ejecutar el comando "composer install".
- Ejecutar el comando "php artisan key:generate"
- Duplicar el archivo .env.example y colocarle el nombre .env para configurar el proyecto.
- En el archivo .env configurar la conexión a la Base de Datos, puede ser Postgresql o Mysql y la BD debe estar creada.
- Ejecutar el comando "php artisan migrate"
- Ejecutar el comando "php artisan db:seed"
- Ejecutar el comando "php artisan serve"
- Ingresar al navegador y colocar la ruta "http://localhost:8000"

