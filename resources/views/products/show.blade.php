@extends('products.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Consultar Producto</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Regresar</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <table class="table table-bordered">
                <tr class="table-active">
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Categoría</th>
                    <th>Marca</th>
                </tr>

                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->categories->name }}</td>
                    <td>{{ $product->trademarks->name }}</td>
                </tr>

                <tr class="table-active">
                    <th colspan="4">Descripción</th>
                </tr>

                <tr>
                    <td colspan="4">{{ $product->detail }}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection